--Task 1--
CREATE DATABASE HMBank;
GO
USE HMBank;
--Task 2--
--Q1--
CREATE TABLE Customers (
   customer_id INT PRIMARY KEY,
   first_name  VARCHAR(50),
   last_name   VARCHAR(50),
   DOB DATE,
   email VARCHAR(100),
   phone_number VARCHAR(20),
   address VARCHAR(255)
   );
CREATE TABLE Accounts(
  account_id INT PRIMARY KEY,
  customer_id INT,
  account_type VARCHAR(20),
  balance DECIMAL(10,2),
   FOREIGN KEY (customer_id) REFERENCES CUSTOMERS(customer_id)
   );
CREATE TABLE Transactions (
   transaction_id INT PRIMARY KEY,
   account_id INT,
   transaction_type VARCHAR(20),
   amount DECIMAL(10,2),
   transaction_date DATE,
    FOREIGN KEY (account_id) REFERENCES Accounts(account_id)
	);
INSERT INTO Customers VALUES
(1,'John','Smith','1990-05-15','john.doe@email.com','1234567890','123 Main St, City1'),
(2,'Raj','Singh','1992-05-25','raj.singh@email.com','1234567890','456 City Street'),
(3,'Ansh','Raj','1994-05-06','ansh.raj@email.com','1234567890','784 Lisbon Palace'),
(4,'Virat','Kohli','1998-02-15','virat.kohli@email.com','1234567890','Chowringhee Bridge'),
(5,'Rohit','Sharma','1980-12-15','rohit.sharma@email.com','1234567890','Market area'),
(6,'Ravi','Kumar','1975-09-28','ravi.kumar@email.com','1234567890','254 Iskon Temple'),
(7,'Kumara','Sen','1995-05-02','kumara.sen@email.com','1234567890','435 Mczee State'),
(8,'Sankalp','Sinha','1998-11-15','sankalp.sinha@email.com','1234567890','987 Disbon Area'),
(9,'Ayushi','Rani','1999-04-15','ayushi.rani@email.com','1234567890','878 Unisoft Palace'),
(10,'Sandeep','Kumar','2000-01-31','sandeep.kumar@email.com','1234567890','147 Street Area')
INSERT INTO Accounts VALUES
(101,1,'savings',1500),
(102,1,'current',500),
(103,1,'savings',2500),
(104,1,'current',3500),
(105,1,'savings',1500),
(106,1,'savings',2400),
(107,1,'current',3500),
(108,1,'savings',800),
(109,1,'current',900),
(110,1,'current',1200)

INSERT INTO Transactions Values
(1,101,'deposit',500.00,'2024-02-06'),
(2,102,'withdrawal',500.00,'2024-05-06'),
(3,103,'deposit',8000.00,'2024-09-25'),
(4,104,'withdrawal',500.00,'2024-05-18'),
(5,105,'deposit',3500.00,'2024-08-06'),
(6,106,'withdrawal',1800.00,'2024-03-06'),
(7,107,'deposit',2500.00,'2024-01-26'),
(8,108,'withdrawal',1500.00,'2024-07-21'),
(9,109,'deposit',4500.00,'2024-09-20'),
(10,110,'withdrawal',7500.00,'2024-04-14')
--Task 2.2.1--
SELECT c.first_name,c.last_name,a.account_type,c.email
FROM Customers c
JOIN Accounts a ON c.customer_id=a.customer_id;
--2.2.2--
SELECT first_name,last_name,transaction_type,amount,transaction_date
FROM Customers
JOIN Accounts ON Customers.customer_id=Accounts.customer_id
JOIN Transactions ON Accounts.account_id=Transactions.account_id;

--2.2.3--
UPDATE Accounts
SET balance=balance + 1200
WHERE account_id= 102;
--2.2.4--
SELECT customer_id, CONCAT(first_name,'',last_name) AS full_name
FROM Customers;
--2.2.5--
DELETE FROM Accounts
WHERE balance=0 AND account_type='savings';
--2.2.6--
SELECT *
FROM Customers
WHERE address LIKE '%Market area%';
--2.2.7--
SELECT account_id, balance
FROM Accounts
WHERE account_id=108;
--2.2.8--
SELECT *
FROM Accounts
WHERE account_type='current' AND balance >1000.00;
--2.2.9--
SELECT *
FROM Transactions
WHERE account_id=107;
--2.2.10--
SELECT account_id,balance*7.5/100 AS interest_accrued
FROM Accounts
WHERE account_type='savings';
--2.2.11--
SELECT *
FROM Accounts
WHERE balance<-1000;
--2.2.12--
SELECT *
FROM Customers
WHERE address NOT LIKE '%Unisoft Palace%';
--Task 3--
--Q1--
SELECT AVG(balance) AS average_balance
FROM Accounts;
--Q2--
SELECT TOP 10 customer_id, account_id,balance
FROM Accounts
ORDER BY balance DESC;
--Q3--

SELECT SUM(t.amount) AS total_deposits 
FROM Transactions t
WHERE t.transaction_type = 'deposit' AND t.transaction_date = '2024-01-06'
GROUP by t.transaction_date
--Q4--
--Oldest Customer--
SELECT TOP 1 customer_id , first_name
FROM Customers
ORDER BY DOB ASC
 ;
--Newest Customer--
SELECT TOP 1 customer_id, first_name
FROM Customers
ORDER BY DOB DESC
;
--Q5--
SELECT t.*,a.account_type
FROM Transactions t
JOIN Accounts a ON t.account_id=a.account_id;
--Q6--
SELECT c.*,a.*
FROM Customers c
JOIN Accounts a ON c.customer_id=a.customer_id;
--Q7--
SELECT c.*,t.*
FROM Customers c
JOIN Accounts a ON c.customer_id=a.customer_id
JOIN Transactions t ON a.account_id=t.account_id
WHERE t.account_id=105;
--Q8--
SELECT customer_id
FROM Accounts
GROUP BY customer_id
HAVING COUNT(account_id) >1;
--Q9--
SELECT t.account_id,
   SUM(CASE WHEN transaction_type='deposit' THEN amount ELSE 0 END)-
   SUM(CASE WHEN transaction_type='withdrawal' THEN amount ELSE 0 END) AS difference
   FROM Transactions t
   GROUP BY t.account_id;

--Q10--
SELECT account_id, AVG(balance) AS average_daily_balance
FROM Accounts
WHERE (SELECT MAX(transaction_date) 
		FROM Transactions 
		WHERE Transactions.account_id = Accounts.account_id) BETWEEN '2024-01-05' AND '2024-09-20'
		GROUP BY account_id;

--Q11--

SELECT account_type,SUM(balance) AS total_balance
FROM Accounts
GROUP BY account_type;

--Q12--
SELECT account_id,COUNT(transaction_id) AS transaction_count
FROM Transactions
GROUP BY account_id
ORDER BY transaction_count DESC;
--Q13--
SELECT c.customer_id,c.first_name,c.last_name,a.account_type,SUM(a.balance) AS aggregate_balance
FROM Customers c
JOIN Accounts a ON c.customer_id=a.customer_id
GROUP BY c.customer_id, c.first_name, c.last_name, a.account_type
ORDER BY aggregate_balance DESC;
--Q14--
SELECT amount, transaction_date, account_id, COUNT(transaction_id) AS duplicate_count
FROM Transactions
GROUP BY amount,transaction_date,account_id
HAVING COUNT(transaction_id)>1;

-- Tasks 4 Subquery Q1--
SELECT customer_id,first_name,last_name
FROM Customers
WHERE customer_id in(SELECT TOP 1 customer_id 
				   FROM Accounts 
				   ORDER BY balance DESC);

--Q2--
SELECT AVG (balance) AS average_balance
FROM Accounts
WHERE customer_id IN (SELECT customer_id 
					  FROM Accounts 
					  GROUP BY customer_id HAVING COUNT(account_id)>1);

--Q3--
SELECT account_id,transaction_type,amount
FROM Transactions
WHERE amount>(SELECT AVG(amount) FROM Transactions);

--Q4-
SELECT customer_id,first_name,last_name
FROM Customers
WHERE customer_id NOT IN (SELECT DISTINCT transaction_id FROM Transactions);

--Q5--
SELECT SUM(balance) AS total_balance
FROM Accounts
WHERE account_id NOT IN (SELECT DISTINCT account_id FROM Transactions);

--Q6--
SELECT account_id,transaction_id, transaction_date
FROM Transactions
WHERE account_id=(SELECT TOP 1 account_id FROM Accounts ORDER BY balance ASC);

--Q7--
SELECT customer_id,first_name,last_name
FROM Customers
WHERE customer_id IN  (SELECT customer_id 
					   FROM Accounts 
					   GROUP BY customer_id 
					   HAVING COUNT(DISTINCT account_type)>1);

--Q8--
SELECT account_type,COUNT(account_id)*100.0/ (SELECT COUNT(account_id) FROM Accounts) AS percentage
FROM Accounts
GROUP BY account_type;

--Q9--
SELECT * FROM Transactions
WHERE account_id IN (SELECT account_id 
					 FROM Accounts 
					 WHERE customer_id = 9);


--Q10--
SELECT account_type,(SELECT SUM(balance) 
FROM Accounts 
WHERE account_type = A.account_type) AS total_balance
FROM Accounts A
GROUP BY account_type;
